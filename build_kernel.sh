#!/bin/bash

export ARCH=arm64
export CROSS_COMPILE=../Utilities/Android-gcc/bin/aarch64-linux-android-
export ANDROID_MAJOR_VERSION=o

make exynos7870-a2corelte_defconfig
make -j64
